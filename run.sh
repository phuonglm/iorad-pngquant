#!/bin/bash
export MAX_FILE_PER_RUN=100
export CAPTURE_DIR=/mnt/storage/iorad/capture
export STATE_DIR=/mnt/storage/iorad/png_quant/state
export CRYPTO_DIR=/mnt/storage/iorad/crypto
export MAX_NEW_DAY=7
export MAX_OLD_DATE=180
nice -n19 yarn start