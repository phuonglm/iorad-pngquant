FROM    node:lts-stretch
RUN     apt-get -y update
RUN     apt-get -y install wget pngquant
RUN     mkdir /app
WORKDIR /app/
ADD     package.json ./
RUN     node -v && yarn -v
RUN     yarn install
ADD     app.js ./app.js
ADD     src ./src
CMD     yarn start