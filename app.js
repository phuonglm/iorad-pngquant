const PngQuant = require('pngquant');
const FindFiles = require('node-find-files');
const Queue = require('better-queue');
const PrettySize = require('prettysize');
const fs = require('fs');

var config = {};
config.max_file_per_day = parseInt(process.env.MAX_FILE_PER_RUN) || 1000;
config.capture_path = process.env.CAPTURE_DIR || './capture';
config.crypto_path = process.env.CRYPTO_DIR || './crypto';
config.state_path = process.env.STATE_DIR || './state';
config.thread = parseInt(process.env.THREAD) || 2

const max_new_day = parseInt(process.env.MAX_NEW_DAY) || 7
const max_old_day = parseInt(process.env.MAX_OLD_DAY) || 30*3

config.max_new_time_diff = max_new_day*24*60*60*1000;
config.max_old_time_diff = max_old_day*24*60*60*1000;
try {
    config.new_png_from_date = parseInt(fs.readFileSync( config.state_path  + '/new_file_ts'));
} catch (err){
    console.log('Timestamp for new file not found, use now instead.');
    config.new_png_from_date = new Date().getTime();
    fs.writeFileSync(config.state_path + '/new_file_ts', new Date().getTime());
}

try {
    config.old_png_from_date = parseInt(fs.readFileSync(config.state_path + '/old_file_ts'));
} catch (err){
    console.log('Timestamp for old file not found, use now instead.');
    config.old_png_from_date = new Date().getTime();
    fs.writeFileSync(config.state_path + '/old_file_ts', new Date().getTime());
}

var total_count = 0, old_file_count = 0, new_file_count = 0, total_size_original = 0, total_size_new=0;

// Convert queue job
var q = new Queue(function(input, cb){
    var input_stream = fs.createReadStream(input.path);
    var output_stream = fs.createWriteStream(input.path + '.png_tmp');
    var png_quant_stream = new PngQuant([256]);
    var last_access = input.stat.atime;
    var last_modified = input.stat.mtime;
    var original_size = input.stat.size;

    output_stream.on('finish', () => {
        var result_stat = fs.statSync(input.path + '.png_tmp');
        total_size_original += original_size;
        total_size_new += result_stat.size;
        if(original_size > result_stat.size) {
            fs.renameSync(input.path + '.png_tmp', input.path);
            fs.utimesSync(input.path, last_access, last_modified);
        } else {
            fs.unlinkSync(input.path + '.png_tmp');
        }
        if(original_size > 0 && result_stat.size > 0){
            console.log("%s this file saved %s, ratio (%s%). Total saved %s, ratio (%s%)",input.path, 
                PrettySize(original_size - result_stat.size),
                ((result_stat.size/original_size)*100).toFixed(2), 
                PrettySize(total_size_original - total_size_new),
                ((total_size_new/total_size_original)*100).toFixed(2)) ;
        }
        fs.writeFileSync(config.state_path + '/' + input.job + '_ts', input.stat.mtimeMs);
        cb();
    });
    png_quant_stream.on('error', function(e){
        console.log(e); 
        cb();
    });
    input_stream.pipe(png_quant_stream).pipe(output_stream);
}, { concurrent: config.thread});

// Select file from last process and add to queue job as long as queue under max number allowed 
function processNewFile(config, cb){
    console.log('Start new png file optomizer for image after date ' + new Date(config.new_png_from_date));

    var search_file_from_date = config.new_png_from_date;
    var new_file_list = [];
    var finder = new FindFiles({
        rootFolder : config.capture_path,
        filterFunction : function (path, stat) {
            return (path.toLowerCase().endsWith('.png')) && (stat.mtimeMs > search_file_from_date) && (stat.mtimeMs - search_file_from_date < config.max_new_time_diff && !fs.existsSync(path.replace(config.capture_path, config.crypto_path) + ".json"));
        }
    });

    finder.on('match', function(strPath, stat) {
        // console.log(strPath);

        new_file_list.push({path: strPath, stat: stat, job: 'new_file'});
    }).on('complete', function() {
        new_file_list.sort(function(a, b){
            return a.stat.mtimeMs - b.stat.mtimeMs;
        })

        if(new_file_list.length > 0){
            console.log('Found ' + new_file_list.length + ' new file from ' + new_file_list[0].stat.mtime + ' to ' + new_file_list[new_file_list.length -1 ].stat.mtime);
        }
        for (var i in new_file_list ){
            if(total_count < config.max_file_per_day || config.new_png_from_date == new_file_list[i].stat.mtimeMs){
                q.push(new_file_list[i]);
                config.new_png_from_date = new_file_list[i].stat.mtimeMs;
                total_count++;
                new_file_count++;
            } else {
                new_file_list = [];
                break;
            }
        }

        console.log('Finished new file select, number of file to process: ' + new_file_count + ' up to date ' + new Date(config.new_png_from_date));

        if (cb){
            cb(config);
        }
    });
    finder.startSearch();
}

function processOlderFile(config, cb){
    console.log('Start old png file optomizer for image before date ' +  new Date(config.old_png_from_date));
    var search_file_before_date = config.old_png_from_date;
    var old_file_list = [];

    var finder = new FindFiles({
        rootFolder : config.capture_path,
        filterFunction : function (path, stat) {

            return (path.toLowerCase().endsWith('.png')) && (stat.mtimeMs < search_file_before_date) && (search_file_before_date - stat.mtimeMs < config.max_old_time_diff && !fs.existsSync(path.replace(config.capture_path, config.crypto_path) + ".json"));
        }
    });

    finder.on('match', function(strPath, stat) {
        // console.log(strPath);
        old_file_list.push({path: strPath, stat: stat, job: 'old_file'});
    }).on('complete', function() {
        old_file_list.sort(function(a, b){
            return b.stat.mtimeMs - a.stat.mtimeMs;
        })

        if(old_file_list.length > 0){
            console.log('Found ' + old_file_list.length + ' old file from ' + old_file_list[0].stat.mtime + ' to ' + old_file_list[old_file_list.length -1 ].stat.mtime);
        }

        for (var i in old_file_list ){
            if(total_count < config.max_file_per_day || config.old_png_from_date == old_file_list[i].stat.mtimeMs){
                q.push(old_file_list[i]);
                config.old_png_from_date = old_file_list[i].stat.mtimeMs;
                total_count++;
                old_file_count++;
            } else {
                old_file_list = [];
                break;
            }
        }

        console.log('Finished old file select, number of file to process: ' + old_file_count + ' down to date ' + new Date(config.old_png_from_date));
        if (cb){
            cb();
        }
    });

    finder.startSearch();
}

console.log(config);
processNewFile(config, processOlderFile);
